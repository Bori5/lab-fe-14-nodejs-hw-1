const express = require('express');
const {promises: fs, mkdirSync, constants} = require('fs');
const path = require('path');
const {sharedFolder, supportedFileTypes} = require('../config');
const regex = new RegExp(`.*\.(${supportedFileTypes.join('|')})`);

const checkFolderAccess = () => fs.access(sharedFolder, constants.R_OK | constants.W_OK);
const checkFolderAccessMiddleware = (req, res, next) => {
  checkFolderAccess()
    .then(next)
    .catch(error => onError(res.status(500), error, 'Server error'));
}

const router = express.Router();

router.post('/files', checkFolderAccessMiddleware, ({body: {filename, content}}, res) => {
  if (!regex.test(filename)) {
    return onError(res.status(400), Error('filename unsupported or empty'));
  }
  if (!content) {
    return onError(res.status(400), Error("Please specify 'content' parameter"));
  }

  fs.writeFile(path.join(sharedFolder, filename), content)
    .then(() => { res.json({message: 'File created successfully'}); })
    .catch(error => onError(res.status(500), error, 'Server error'));
});

router.put('/files', checkFolderAccessMiddleware, ({body: {filename, content}}, res) => {
  fs.open(path.join(sharedFolder, filename), 'r+')
    .then(async (handle) => {
      try {
        await handle.writeFile(content);
        res.json({message: `Success 🎉 file ${filename} updated!`});
      } finally {
        await handle.close();
      }
    })
    .catch(error => {
      if (error.code === 'ENOENT' && error.syscall === 'open') {
        onError(res.status(400), error, `File "${filename}" not found`);
      } else onError(res.status(500), error, 'Server error');
    });
});

router.get('/files', (req, res) => {
  fs.readdir(sharedFolder)
    .then(files => { res.json({message: 'Success', files}); })
    .catch(error => onError(res.status(500), error, 'Server error'));
});

router.get('/files/:file', checkFolderAccessMiddleware, (req, res) => {
  const filename = req.params.file;
  if (!regex.test(filename)) {
    return onError(res.status(400), Error(`No file with '${filename}' filename found`));
  }

  fs.open(path.join(sharedFolder, filename), 'r')
    .then(async (handle) => {
      try {
        const [{birthtime}, content] = await Promise.all([handle.stat(), handle.readFile('utf8')]);
        res.json({
          message: 'Success',
          filename,
          content,
          extension: filename.split('.').pop(),
          uploadedDate: birthtime
        });
      } finally {
        await handle.close();
      }
    })
    .catch(error => {
      if (error.code === 'ENOENT' && error.syscall === 'open') {
        onError(res.status(400), Error(`No file with ${filename} filename found`));
      } else onError(res.status(500), error, 'Server error');
    });
});

router.delete('/files/:file', checkFolderAccessMiddleware, (req, res) => {
  const fileName = req.params.file;
  fs.unlink(path.join(path.join(sharedFolder, fileName)))
    .then(() => { res.json({message: 'File deleted successfully'}); })
    .catch(error => {
      if (error.code === 'ENOENT' && error.syscall === 'unlink') {
        onError(res.status(400), Error(`No file with ${fileName} filename found`));
      } else onError(res.status(500), error, 'Server error');
    });
});

const onError = (res, error, message = null) => {
  res.json({message: message || error.message});
  console.error(error);
};

checkFolderAccess().catch(() => mkdirSync(sharedFolder));

module.exports = router;
