const express = require('express');
const logger = require('morgan');
const path = require('path');
const cors = require('cors');

const apiRouter = require('./routes/api');
const indexRouter = require('./routes/index');

const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use(cors());
app.use('/api', apiRouter);

module.exports = app;
