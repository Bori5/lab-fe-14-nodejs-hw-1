const path = require('path');

module.exports = {
  port: 8080,
  sharedFolder: path.resolve(__dirname, 'storage'),
  supportedFileTypes: ['log', 'txt', 'json', 'yaml', 'xml', 'js'],
};
